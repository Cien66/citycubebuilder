
#pragma once

#include "UObject/Interface.h"
#include "../Resources/CResourcesActor.h"
#include "CSetGroundResourcesInterface.generated.h"

UINTERFACE(MinimalAPI)
class UCSetGroundResourcesInterface : public UInterface
{
	GENERATED_BODY()
};

class ICSetGroundResourcesInterface
{
	GENERATED_BODY()

public:
	virtual void SetGroundResources(class ACResourcesActor* InGroundResources) {}
};
