#pragma once

#include "GameFramework/Actor.h"
#include "../Data/CGameplayEnums.h"
#include "CEconomyControllerActor.generated.h"

UCLASS(ClassGroup = "City cube builder")
class ACEconomyControllerActor: public AActor
{
	GENERATED_BODY()

public:
	ACEconomyControllerActor();

	void SetWarehouseController(const TWeakObjectPtr<class ACWarehousesControllerActor> InWarehousesController) { WarehousesController = InWarehousesController; }

	void AddBuilding(const TWeakObjectPtr<class ACBuildingActor> &InBuilding);
	void RemoveBuilding(const TWeakObjectPtr<class ACBuildingActor> &InBuilding);

private:
	void AddMine(const TWeakObjectPtr<class ACMineBuildingActor> &InMine);
	void RemoveMine(const TWeakObjectPtr<class ACMineBuildingActor> &InMine);

	TArray<const TWeakObjectPtr<class ACMineBuildingActor>> Mines;

	TWeakObjectPtr<class ACWarehousesControllerActor> WarehousesController{nullptr};
};
