
#pragma once

#include "GameFramework/Actor.h"
#include "../Data/CGameplayEnums.h"
#include "CWarehousesControllerActor.generated.h"


DECLARE_MULTICAST_DELEGATE_OneParam(FOnUpdateInWarehouseDelegate, const ACWarehousesControllerActor*);

UCLASS(ClassGroup = "City cube builder")
class ACWarehousesControllerActor: public AActor
{
	GENERATED_BODY()

public:
	ACWarehousesControllerActor();

	FOnUpdateInWarehouseDelegate OnUpdateProductsDelegate;

	FORCEINLINE float GetProductsByType(const EResourcesType& InResourcesType)const { return ProductsInMagazines[InResourcesType]; }
	FORCEINLINE float GetMaxLimitProducts()const { return MaxProductsInMagazines; }

	void AddBuilding(class ACBuildingActor* InBuilding);
	void RemoveBuilding(class ACBuildingActor* InBuilding);

	UFUNCTION()
		void OnAddedNewProducts(const EResourcesType& InResourcesType, float& InCount, const class ACBuildingActor* InBuiilding);

	UFUNCTION()
		void OnUsedProducts(const EResourcesType& InResourcesType, float& InCount, const ACBuildingActor* InBuiilding);

	bool UseResources(const struct FCBuilding& InBuilding);
	bool ChceckEnoughtResourcesToBuild(const struct FCBuilding& InBuilding) const; 

	void AddWarehouse(TWeakObjectPtr<class ACWarehouseBuildingActor> InWarehouse);
	void RemoveWarehouse(TWeakObjectPtr<class ACWarehouseBuildingActor> InWarehouse);

private:
	struct FDelegatesHandlerWarehouse
	{
		FDelegateHandle OnDeliverProductDelegateHandle;
		FDelegateHandle OnTakeProductDelegateHandle;
	};

	float GetAllCountResourcesByType(const EResourcesType& InResourcesType) const;
	void OnBuildNeedResources(const EResourcesType& InResourcesType, float& InCount, const class ACBuildingActor* InBuilding);

	TMap<TWeakObjectPtr<class ACWarehouseBuildingActor>, const FDelegatesHandlerWarehouse> Warehouses;
	TMap<const EResourcesType, float> ProductsInMagazines;

	float MaxProductsInMagazines;
};
