
#include "CWarehousesControllerActor.h"
#include "../Data/CGameplayStuctures.h"
#include "../Building/CWarehouseBuildingActor.h"
#include "../Components/Buildings/CWarehouseComponent.h"


#pragma region PUBLIC

ACWarehousesControllerActor::ACWarehousesControllerActor()
{
	ProductsInMagazines.Add(EResourcesType::E_NONE, 0);
	ProductsInMagazines.Add(EResourcesType::E_Green, 0);
	ProductsInMagazines.Add(EResourcesType::E_Blue, 0);
	ProductsInMagazines.Add(EResourcesType::E_Red, 0);
	ProductsInMagazines.Add(EResourcesType::E_Purple, 0);
}

#pragma endregion

#pragma region PRIVATE

void ACWarehousesControllerActor::AddWarehouse(TWeakObjectPtr<class ACWarehouseBuildingActor> InWarehouse)
{
	if(InWarehouse.IsValid())
	{
		FDelegatesHandlerWarehouse DHW;
		DHW.OnDeliverProductDelegateHandle = InWarehouse->OnDeliverProductDelegate.AddUObject(this, &ACWarehousesControllerActor::OnAddedNewProducts);
		DHW.OnTakeProductDelegateHandle = InWarehouse->OnTakeProductDelegate.AddUObject(this, &ACWarehousesControllerActor::OnUsedProducts);

		MaxProductsInMagazines += InWarehouse->GetWarehouseComponent()->GetMaxProductInBuilding();

		Warehouses.Add(InWarehouse, DHW);
	}
}

void ACWarehousesControllerActor::RemoveWarehouse(TWeakObjectPtr<class ACWarehouseBuildingActor> InWarehouse)
{
	if(const FDelegatesHandlerWarehouse* DWH = Warehouses.Find(InWarehouse))
	{
		InWarehouse->OnDeliverProductDelegate.Remove(DWH->OnDeliverProductDelegateHandle);
		InWarehouse->OnTakeProductDelegate.Remove(DWH->OnTakeProductDelegateHandle);

		MaxProductsInMagazines -= InWarehouse->GetWarehouseComponent()->GetMaxProductInBuilding();
		Warehouses.Remove(InWarehouse);		
	}
}

bool ACWarehousesControllerActor::UseResources(const FCBuilding& InBuilding) 
{
	if(!ChceckEnoughtResourcesToBuild(InBuilding))
		return false;

	for(const auto& Pair : InBuilding.ResourcesNeedToBuildBuilding)
		ProductsInMagazines[Pair.Key] -= Pair.Value;

	return true;
}

bool ACWarehousesControllerActor::ChceckEnoughtResourcesToBuild(const FCBuilding& InBuilding) const
{
	for(const auto& Pair : InBuilding.ResourcesNeedToBuildBuilding)
		if(ProductsInMagazines[Pair.Key] < Pair.Value)
			return false;

	return true;
}

void ACWarehousesControllerActor::OnAddedNewProducts(const EResourcesType& InResourcesType, float& InCount, const ACBuildingActor* InBuiilding)
{
	float NewCount = FMath::Clamp(ProductsInMagazines[InResourcesType] + InCount, 0.f, MaxProductsInMagazines);
	ProductsInMagazines[InResourcesType] = NewCount;

	OnUpdateProductsDelegate.Broadcast(this);
}

void ACWarehousesControllerActor::OnBuildNeedResources(const EResourcesType& InResourcesType, float& InCount, const ACBuildingActor* InBuilding)
{
	if(!InBuilding)
	{
		InCount = 0;
		return;
	}

	float SqrDistance = -1.f;
	TWeakObjectPtr<ACWarehouseBuildingActor> NearestWarehouse{nullptr};

	for(const auto& Warehouse : Warehouses)
	{
		float CurentSqrDistance = FVector::DistSquared(Warehouse.Key->GetActorLocation(), InBuilding->GetActorLocation());

		if(CurentSqrDistance < SqrDistance || SqrDistance < 0.f)
		{
			SqrDistance = CurentSqrDistance;
			NearestWarehouse = Warehouse.Key;
		}
	}

	NearestWarehouse->AddBuildingWaitingForResources(InBuilding, InResourcesType, InCount);
}

void ACWarehousesControllerActor::OnUsedProducts(const EResourcesType& InResourcesType, float& InCount, const ACBuildingActor* InBuiilding)
{
	InCount = FMath::Min(InCount, ProductsInMagazines[InResourcesType]);
	ProductsInMagazines[InResourcesType] -= InCount;

	OnUpdateProductsDelegate.Broadcast(this);
}

float ACWarehousesControllerActor::GetAllCountResourcesByType(const EResourcesType& InResourcesType) const
{
	return ProductsInMagazines[InResourcesType];
}

#pragma endregion