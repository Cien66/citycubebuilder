
#include "CEconomyControllerActor.h"
#include "CWarehousesControllerActor.h"

#include "../Building/CWarehouseBuildingActor.h"
#include "../Building/CMineBuildingActor.h"


#pragma region PUBLIC

ACEconomyControllerActor::ACEconomyControllerActor()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ACEconomyControllerActor::AddBuilding(const TWeakObjectPtr<class ACBuildingActor>& InBuilding)
{
	switch(InBuilding->GetParameters().BuildingType)
	{
		case EBuildingType::E_Warehouse: if(WarehousesController.IsValid()) WarehousesController->AddWarehouse(Cast<ACWarehouseBuildingActor>(InBuilding)); break;
		case EBuildingType::E_Mining: AddMine(Cast<ACMineBuildingActor>(InBuilding)); break;
		default:
		break;
	}
}

void ACEconomyControllerActor::RemoveBuilding(const TWeakObjectPtr<class ACBuildingActor>& InBuilding)
{
	switch(InBuilding->GetParameters().BuildingType)
	{
		case EBuildingType::E_Warehouse: if(WarehousesController.IsValid()) WarehousesController->RemoveWarehouse(Cast<ACWarehouseBuildingActor>(InBuilding)); break;
		case EBuildingType::E_Mining: RemoveMine(Cast<ACMineBuildingActor>(InBuilding)); break;
		default:
		break;
	}
}

#pragma endregion

#pragma region PROTECTED
#pragma endregion

#pragma region PRIVATE

void ACEconomyControllerActor::AddMine(const TWeakObjectPtr<class ACMineBuildingActor>& InMine)
{
	if(InMine.IsValid() && WarehousesController.IsValid())
	{
		InMine->OnCreateNewProductsDelegateHandle = InMine->OnCreateNewProductsDelegate.AddUObject(WarehousesController.Get(), &ACWarehousesControllerActor::OnAddedNewProducts);
		InMine->OnNeedResourcesDelegateHandle = InMine->OnNeedResourcesDelegate.AddUObject(WarehousesController.Get(), &ACWarehousesControllerActor::OnUsedProducts);
	}
}

void ACEconomyControllerActor::RemoveMine(const TWeakObjectPtr<class ACMineBuildingActor>& InMine)
{
	if(Mines.Find(InMine)>=0)
	{
		InMine->OnCreateNewProductsDelegate.Remove(InMine->OnCreateNewProductsDelegateHandle);
		InMine->OnNeedResourcesDelegate.Remove(InMine->OnNeedResourcesDelegateHandle);
	}
}

#pragma endregion