#pragma once

#include "CGameplayEnums.generated.h"

UENUM(BlueprintType)
enum class EBuildingType: uint8
{
	E_Mining = 0		UMETA(DisplayName = "Mining"),
	E_Processing		UMETA(DisplayName = "Processing"),
	E_Residential		UMETA(DisplayName = "Residential"),
	E_Warehouse			UMETA(DisplayName = "Warehouse")
};

UENUM(BlueprintType)
enum class EResourcesType: uint8
{
	E_NONE = 0			UMETA(DisplayName = "NONE"),
	E_Green 			UMETA(DisplayName = "Green"),
	E_Blue				UMETA(DisplayName = "BLue"),
	E_Red				UMETA(DisplayName = "Red"),
	E_Purple			UMETA(DisplayName = "Purple")
};

UENUM(BlueprintType)
enum class EGameplayState: uint8
{
	E_Free = 0			UMETA(DisplayName = "Free"),
	E_Build				UMETA(DisplayName = "Build"),
	E_ViewDetails		UMETA(DisplayName = "ViewDetails")
};