
#pragma once

#include "CGameplayStuctures.h"
#include "CGameplayEnums.h"
#include "../Building/CBuildingActor.h"

DECLARE_MULTICAST_DELEGATE_ThreeParams(FOnUpdateProductDelegate, const EResourcesType&, float&, const ACBuildingActor*);