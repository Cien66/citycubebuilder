
#pragma once

#include "Engine/DataTable.h"
#include "CGameplayStuctures.h"
#include "CBuildingDataTable.generated.h"

USTRUCT(BlueprintType)
struct FCBuildingDataTable: public FTableRowBase
{
	GENERATED_BODY()

public:
	FCBuildingDataTable();
	~FCBuildingDataTable();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingDataTable")
		FLinearColor ButtonColor{FLinearColor::White};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BuildingDataTable")
		FCBuilding Building;
};
