
#pragma once

#include "Engine/DataTable.h"
#include "CGameplayEnums.h"
#include "CRoadDataTable.generated.h"

USTRUCT(BlueprintType)
struct FCRoadDataTable: public FTableRowBase
{
	GENERATED_BODY()

public:
	FCRoadDataTable();
	~FCRoadDataTable();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RoadDataTable")
		FString Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RoadDataTable")
		FLinearColor ButtonColor{FLinearColor::White};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RoadDataTable")
		float SpeedMoveOnRoud{300.f};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RoadDataTable")
		TMap<EResourcesType, float> ResourcesNeedToBuildRoad;
};
