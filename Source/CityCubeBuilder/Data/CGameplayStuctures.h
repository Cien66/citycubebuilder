#pragma once

#include "CGameplayEnums.h"
#include "CGameplayStuctures.generated.h"




USTRUCT(BlueprintType)
struct FCProduce
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Produce")
		EResourcesType ProductType{EResourcesType::E_Green};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Produce")
		int MaxProductsInBuilding{1};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Produce")
		float TimeCycleWork = {30.f};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Produce")
		int CountProductByCycleWork{1};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Produce")
		int FreeProductToExtract = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Produce")
		EResourcesType ResourcesNeedInGround{EResourcesType::E_NONE};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Produce")
		 TMap<EResourcesType, float> ResourcesNeedToCreateProduct;
};

USTRUCT(BlueprintType)
struct FCBuilding
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Building")
		FString Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Building")
		TSubclassOf<class ACBuildingActor> BuildingClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Building")
		EBuildingType BuildingType{EBuildingType::E_Mining};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Building")
		int MaxPeopleInBuilding{8};
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Building")
		int MaxResourcesInBuildingPerType{1};
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Building")
		TMap<EResourcesType, float> ResourcesNeedToBuildBuilding;
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Building")
		FVector2D BuildSize{FVector2D::UnitVector};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Building")
		class UStaticMesh* Mesh{nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Building")
		class UMaterial* Material{nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Building")
		FCProduce Produce;
};