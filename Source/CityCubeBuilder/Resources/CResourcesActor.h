
#pragma once

#include "GameFramework/Actor.h"
#include "../Data/CGameplayEnums.h"
#include "CResourcesActor.generated.h"

DECLARE_DELEGATE(FOnEndResourcesDelegate)

UCLASS(ClassGroup = "City cube builder")
class ACResourcesActor: public AActor
{
	GENERATED_BODY()

public:
	ACResourcesActor();

	FOnEndResourcesDelegate EndResourcesDelegate;

	UFUNCTION(BlueprintPure, Category = "Resources")
		FORCEINLINE  int GetLeftResources() const { return ResourcesCount; }
	UFUNCTION(BlueprintPure, Category = "Resources")
		FORCEINLINE EResourcesType GetResourcesType() const { return ResourcesType; }

	int Extract(int InExtractCount);

protected:
	UPROPERTY(EditAnywhere, Category = "Resources")
		class UBoxComponent* BoxCollision{nullptr};

	UPROPERTY(EditAnywhere, Category = "Resources")
		int ResourcesCount{1000};

	UPROPERTY(EditAnywhere, Category = "Resources")
		EResourcesType ResourcesType{EResourcesType::E_Green};

	FTimerHandle DelayDestroy;
};
