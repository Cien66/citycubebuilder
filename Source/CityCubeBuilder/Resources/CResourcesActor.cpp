
#include "CResourcesActor.h"
#include <Runtime/Engine/Classes/Components/BoxComponent.h>


ACResourcesActor::ACResourcesActor()
{
	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	RootComponent = BoxCollision;
}

int ACResourcesActor::Extract(int InExtractCount)
{
	int RealExtract = InExtractCount < ResourcesCount ? InExtractCount : ResourcesCount;
	
	ResourcesCount -= RealExtract;
	if(ResourcesCount <= 0)
	{
		EndResourcesDelegate.ExecuteIfBound();
		
		GetWorldTimerManager().SetTimer(DelayDestroy, [this]() {Destroy(); }, 1.0f, false); //Delay destroy actor
	}

	return RealExtract;
}
