#pragma once

#include "Components/ActorComponent.h"
#include "../../Controllers/CWarehousesControllerActor.h"
#include "CUIHandlerComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateParametersDelegate, const UCUIHandlerComponent*, InUIHandler);

UCLASS(ClassGroup = ("City cube builder"), meta = (BlueprintSpawnableComponent))
class UCUIHandlerComponent: public UActorComponent
{
	GENERATED_BODY()

public:
	UCUIHandlerComponent();

	UPROPERTY(BlueprintAssignable, Category = "UIHandler")
		FOnUpdateParametersDelegate OnUpdateResourcesInWarehouseDelegate;

	void Init(const TWeakObjectPtr<class ACWarehousesControllerActor>& InWarehousesController);

	UFUNCTION(BlueprintCallable, Category = "UIHandler")
		void Refresh();

	UFUNCTION(BlueprintPure, Category = "UIHandler")
		FORCEINLINE float GetProductsByType(const EResourcesType& InResourcesType)const { return WarehousesController.IsValid() ? WarehousesController->GetProductsByType(InResourcesType) : 0.f; }
	UFUNCTION(BlueprintPure, Category = "UIHandler")
		FORCEINLINE float GetMaxLimitProducts()const { return WarehousesController.IsValid() ? WarehousesController->GetMaxLimitProducts() : 0.f; }

private:
	void Bind(const TWeakObjectPtr<class ACWarehousesControllerActor>& InWarehousesController);
	void Unbind(const TWeakObjectPtr<class ACWarehousesControllerActor>& InWarehousesController);

	UFUNCTION()
	void OnUpdateResourcesInWarehouse(const class ACWarehousesControllerActor* InWarehousesController);
	
	TWeakObjectPtr<class ACWarehousesControllerActor> WarehousesController{nullptr};

	FDelegateHandle OnUpdateProductDelegateHandle;
};
