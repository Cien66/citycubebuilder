
#include "CUIHandlerComponent.h"
#include "../../Controllers/CWarehousesControllerActor.h"

#pragma region PUBLIC

UCUIHandlerComponent::UCUIHandlerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UCUIHandlerComponent::Init(const TWeakObjectPtr<ACWarehousesControllerActor>& InWarehousesController)
{
	Unbind(WarehousesController);

	WarehousesController = InWarehousesController;

	Bind(WarehousesController);
}

void UCUIHandlerComponent::Refresh()
{
	OnUpdateResourcesInWarehouseDelegate.Broadcast(this);
}

#pragma endregion

#pragma region PRIVATE

void UCUIHandlerComponent::Bind(const TWeakObjectPtr<ACWarehousesControllerActor>& InWarehousesController)
{
	if(InWarehousesController.IsValid())
		OnUpdateProductDelegateHandle = InWarehousesController->OnUpdateProductsDelegate.AddUObject(this, &UCUIHandlerComponent::OnUpdateResourcesInWarehouse);
}

void UCUIHandlerComponent::Unbind(const TWeakObjectPtr<ACWarehousesControllerActor>& InWarehousesController)
{
	if(InWarehousesController.IsValid())
		InWarehousesController->OnUpdateProductsDelegate.Remove(OnUpdateProductDelegateHandle);
}

void UCUIHandlerComponent::OnUpdateResourcesInWarehouse(const ACWarehousesControllerActor* InWarehousesController)
{
	if(InWarehousesController == WarehousesController)
		OnUpdateResourcesInWarehouseDelegate.Broadcast(this);
}

#pragma endregion