#pragma once

#include "Components/ActorComponent.h"
#include "CBuildSystemComponent.generated.h"

DECLARE_DELEGATE_OneParam(FOnUpdateBuildingLocationDelegate , FVector&);

/*
* Sorry za teoretyzowanie, ale to powinien by� aktor z componentami dla UCBuildingBuildSystem i UCPathBuildSystem
* z braku czasu robi� tylko building i pomijam aktory kt�ry powinien mie� warehaouse i 
* system sprawdzani wystarczaj�cej liczby resourc�w a komponenty tylko system ghosta  i budowy
*/

UCLASS( ClassGroup= "City cube builder", meta=(BlueprintSpawnableComponent) )
class UCBuildSystemComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UCBuildSystemComponent();

	FOnUpdateBuildingLocationDelegate OnUpdateBuildingLocationDelegate;

	void CreateGhostBuilding(const FString& InName);
	TWeakObjectPtr<class ACBuildingActor> BuildRealBuilding();

	void SetBuildingFactory(const TWeakObjectPtr<class ACBuildingFactoryActor>& InBuildingFactory);
	void SetWarehousesController(const TWeakObjectPtr<class ACWarehousesControllerActor>& InWarehousesControlle);

protected:
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	bool IsPlaceAndResourcesForBuilding(class ACResourcesActor*& OutResources) const;
	bool AreNeededResourcesAndNoObstacle(const TArray<FHitResult>& InHits, ACResourcesActor*& OutResources) const;

	UPROPERTY(EditAnywhere, Category = "BuildSystemComponent", meta = (AllowPrivateAccess = "true"))
		TEnumAsByte<ECollisionChannel> BuildTraceChannel{ECollisionChannel::ECC_Camera};

	UPROPERTY(EditAnywhere, Category = "BuildSystemComponent", meta = (AllowPrivateAccess = "true"))
		float GridSize{50.f};
	
	UPROPERTY(EditAnywhere, Category = "BuildSystemComponent", meta = (AllowPrivateAccess = "true"))
		class UDataTable* BuildingDataTable{nullptr};

	TWeakObjectPtr<class ACBuildingActor> BuildingGhost{nullptr};
	TWeakObjectPtr<class ACBuildingFactoryActor> BuildingFactory{nullptr};
	TWeakObjectPtr<class ACWarehousesControllerActor> WarehousesController{nullptr};
		
	FString CurrentBuildingRowName;
};
