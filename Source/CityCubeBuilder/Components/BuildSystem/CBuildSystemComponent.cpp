
#include "CBuildSystemComponent.h"
#include "../../Building/CBuildingActor.h"
#include "../../Factory/CBuildingFactoryActor.h"
#include "../../Controllers/CWarehousesControllerActor.h"

#include "../../Resources/CResourcesActor.h"
#include "../../Interfaces/CSetGroundResourcesInterface.h"

#include <Runtime/Engine/Classes/Kismet/KismetMathLibrary.h>
#include <Runtime/Engine/Classes/Components/BoxComponent.h>
#include <Runtime/Engine/Classes/Engine/DataTable.h>


#pragma region PUBLIC

UCBuildSystemComponent::UCBuildSystemComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UCBuildSystemComponent::CreateGhostBuilding(const FString& InName)
{
	if(BuildingGhost.IsValid())
		BuildingGhost->Destroy();

	if(BuildingFactory.IsValid() && WarehousesController.IsValid())
	{
		CurrentBuildingRowName = InName;
		BuildingGhost = BuildingFactory->Spawn(InName, FTransform::Identity, true);
		if(BuildingGhost.IsValid())
		{
			BuildingGhost->SetIsGhost(true);

			PrimaryComponentTick.bCanEverTick = true;
		}
	}
}

TWeakObjectPtr<ACBuildingActor> UCBuildSystemComponent::BuildRealBuilding()
{
	if(CurrentBuildingRowName.IsEmpty() || !WarehousesController.IsValid() || !BuildingFactory.IsValid() || !BuildingGhost.IsValid())
		return nullptr;

	if(WarehousesController->ChceckEnoughtResourcesToBuild(BuildingGhost->GetParameters()))
	{
		class ACResourcesActor* ResourcesInGround = nullptr;
		if(WarehousesController->ChceckEnoughtResourcesToBuild(BuildingGhost->GetParameters()) && IsPlaceAndResourcesForBuilding(ResourcesInGround))
		{
			FActorSpawnParameters ASP;
			ASP.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			if(ACBuildingActor* RealBuilding = BuildingFactory->Spawn(CurrentBuildingRowName, BuildingGhost->GetActorTransform(), false))
			{
				RealBuilding->SetActorTransform(BuildingGhost->GetActorTransform());

				WarehousesController->UseResources(BuildingGhost->GetParameters());

				if(RealBuilding->GetParameters().BuildingType == EBuildingType::E_Mining)
					if(ICSetGroundResourcesInterface* SetGroundResources = Cast<ICSetGroundResourcesInterface>(RealBuilding))
						SetGroundResources->SetGroundResources(ResourcesInGround);
			}
		}
	}

	return nullptr;
}

void UCBuildSystemComponent::SetWarehousesController(const TWeakObjectPtr<class ACWarehousesControllerActor>& InWarehousesController)
{
	WarehousesController = InWarehousesController;
}

void UCBuildSystemComponent::SetBuildingFactory(const TWeakObjectPtr<class ACBuildingFactoryActor>& InBuildingFactory)
{
	BuildingFactory = InBuildingFactory;

	if(BuildingFactory.IsValid())
		BuildingFactory->SetDataTable(BuildingDataTable);
}

#pragma endregion

#pragma region PROTECTED

void UCBuildSystemComponent::BeginPlay()
{
	Super::BeginPlay();

	PrimaryComponentTick.bCanEverTick = false;
}

void UCBuildSystemComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(BuildingGhost.IsValid())
	{
		FVector NewLocation;
		OnUpdateBuildingLocationDelegate.ExecuteIfBound(NewLocation);

		FVector SnappedLocation = UKismetMathLibrary::Vector_SnappedToGrid(NewLocation, GridSize);
		SnappedLocation.Z = NewLocation.Z + BuildingGhost->GetBoxComponent()->GetScaledBoxExtent().Z;

		ACResourcesActor* Resources = nullptr;
		BuildingGhost->SetActorLocation(SnappedLocation);		
		BuildingGhost->UpdateGhostOnCollisionBuild(IsPlaceAndResourcesForBuilding(Resources));
	}
}

#pragma endregion

#pragma region PRIVATE

bool UCBuildSystemComponent::IsPlaceAndResourcesForBuilding(ACResourcesActor*& OutResources) const
{
	if(UWorld* World = GetWorld())
	{
		const FVector& Extent = BuildingGhost->GetBoxComponent()->GetScaledBoxExtent();
		const FVector& Location = BuildingGhost->GetActorLocation();

		TArray<FHitResult> Hits;
		FCollisionShape CollisionShape = FCollisionShape::MakeBox(Extent);

		FVector Start = BuildingGhost->GetActorLocation() + (FVector::DownVector * Extent.Z) - FVector(0.f, 0.f, 10.f); // Trace from under ground
		FVector End = BuildingGhost->GetActorLocation();

		World->SweepMultiByChannel(Hits, Start, End, BuildingGhost->GetActorQuat(), BuildTraceChannel, CollisionShape);

		return AreNeededResourcesAndNoObstacle(Hits, OutResources);
	}

	return false;
}

bool UCBuildSystemComponent::AreNeededResourcesAndNoObstacle(const TArray<FHitResult>& InHits, ACResourcesActor*& OutResources) const
{
	bool AreNeededResourcses = BuildingGhost->GetParameters().Produce.ResourcesNeedInGround == EResourcesType::E_NONE;

	for(const FHitResult Hit : InHits)
	{
		if(ACResourcesActor* Resources = Cast<ACResourcesActor>(Hit.GetActor()))
		{
			if(Resources->GetResourcesType() == BuildingGhost->GetParameters().Produce.ResourcesNeedInGround)
			{
				OutResources = Cast<ACResourcesActor>(Hit.GetActor());
				AreNeededResourcses = true;
			}
		} else
			return false; // is obstacle
	}

	return AreNeededResourcses;
}

#pragma endregion