
#pragma once

#include "Components/ActorComponent.h"
#include "../../Data/CGameplayStuctures.h"
#include "../../Data/CGameplayEnums.h"
#include "CProduceComponent.generated.h"

DECLARE_DELEGATE(FOnEndResourcesDelegate)
DECLARE_DELEGATE_OneParam(FOnReadyProductDelegate, float);
DECLARE_DELEGATE_OneParam(FOnUpdateProduceTimeDelegate, int);

UCLASS( ClassGroup=("City cube builder"), meta=(BlueprintSpawnableComponent) )
class UCProduceComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UCProduceComponent();

	FOnEndResourcesDelegate OnEndResourcesDelegate;
	FOnReadyProductDelegate OnReadyProductDelegate;
	FOnUpdateProduceTimeDelegate OnUpdateProduceTimeDelegate;

	UFUNCTION(BlueprintPure, Category = "Produce")
	FORCEINLINE FCProduce GetParameters() const { return Produce; }
	void SetParameters(const struct FCProduce& InProduce);

	void RunProduce();

	UFUNCTION(BlueprintPure, Category = "Produce")
		float GetReadyProducts() { return ReadyProduct; }
	float GiveReadyProduct();

	void AddResources(const TMap<EResourcesType, float>& InResources);

	void SetResourcesInGround(const TWeakObjectPtr<class ACResourcesActor>& InResources);

protected:
	virtual void TickComponent(float InDeltaTime, ELevelTick InTickType, FActorComponentTickFunction* InThisTickFunction) override;


private:
	bool CanProduce() const;
	bool AreReasourcesInGround() const;
	bool AreResourcesInBuilding() const;

	UPROPERTY(EditAnywhere, Category = "Produce")
		FCProduce Produce;

	TWeakObjectPtr<class ACResourcesActor> ResourcesInGround{nullptr};

	float LeftFreeProductsToExtract = 0;
	float ReadyProduct = 0;

	UFUNCTION()
		void Extract();

	float LeftProduceTime = 0.f;
	float LastLeftProduceTime = 0.f;

	bool bIsProduce = false;

	TMap<EResourcesType, int> ResourcesInBuild;
};
