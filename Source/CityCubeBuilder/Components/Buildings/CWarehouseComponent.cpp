
#include "CWarehouseComponent.h"

UCWarehouseComponent::UCWarehouseComponent()
{
}

void UCWarehouseComponent::AddProducts(const EResourcesType& InResourcesType, float InProducts)
{
	OnAddProductsWarehouseDelegate.Broadcast(InResourcesType, InProducts);
}

int UCWarehouseComponent::TakeProducts(const EResourcesType& InResourcesType, float InProductToTake)
{
	OnTakeProductsWarehouseDelegate.Broadcast(InResourcesType, InProductToTake);

	return InProductToTake;
}
