#pragma once

#include "Components/ActorComponent.h"
#include "../../Data/CGameplayEnums.h"
#include "../../Data/CGameplayStuctures.h"
#include "CWarehouseComponent.generated.h"

DECLARE_MULTICAST_DELEGATE_TwoParams(FOnUpdateProductsWarehouseDelegate, const EResourcesType&, float&);

UCLASS(ClassGroup = "City cube builder", meta = (BlueprintSpawnableComponent))
class UCWarehouseComponent: public UActorComponent
{
	GENERATED_BODY()

public:
	UCWarehouseComponent();

	FOnUpdateProductsWarehouseDelegate OnAddProductsWarehouseDelegate;
	FOnUpdateProductsWarehouseDelegate OnTakeProductsWarehouseDelegate;

	void SetParameters(const int& InMaxProductInBuild) { MaxProductInBuild = InMaxProductInBuild; }
	FORCEINLINE float GetMaxProductInBuilding() const { return MaxProductInBuild; }

	void AddProducts(const EResourcesType& InResourcesType, float InProducts = 1);
	int TakeProducts(const EResourcesType& InResourcesType, float InProductToTake = 1);

protected:
	float MaxProductInBuild = 0;
};
