
#include "CProduceComponent.h"
#include "../../Resources/CResourcesActor.h"

#pragma region PUBLIC

UCProduceComponent::UCProduceComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	ResourcesInBuild.Add(EResourcesType::E_NONE, 0);
	ResourcesInBuild.Add(EResourcesType::E_Green, 0);
	ResourcesInBuild.Add(EResourcesType::E_Blue, 0);
	ResourcesInBuild.Add(EResourcesType::E_Red, 0);
	ResourcesInBuild.Add(EResourcesType::E_Purple, 0);
}

void UCProduceComponent::SetParameters(const FCProduce& InProduce)
{
	Produce = InProduce;  
	
	LeftFreeProductsToExtract = Produce.FreeProductToExtract;

	RunProduce();
}

void UCProduceComponent::RunProduce()
{
	if(CanProduce())
	{
		bIsProduce = true;
		LeftProduceTime = Produce.TimeCycleWork;
		LastLeftProduceTime = LeftProduceTime;
	}
}

float UCProduceComponent::GiveReadyProduct()
{
	float Give = ReadyProduct;

	ReadyProduct = 0;

	RunProduce();

	return Give;
}

void UCProduceComponent::AddResources(const TMap<EResourcesType, float>& InResources)
{
	for(auto Resource : InResources)
		ResourcesInBuild[Resource.Key] += Resource.Value;

	RunProduce();
}

void UCProduceComponent::SetResourcesInGround(const TWeakObjectPtr<class ACResourcesActor>& InResources)
{
	if(ResourcesInGround.IsValid())
		ResourcesInGround->EndResourcesDelegate.Unbind();

	ResourcesInGround = InResources;

	if(ResourcesInGround.IsValid())
		ResourcesInGround->EndResourcesDelegate.BindLambda([this]() {OnEndResourcesDelegate.ExecuteIfBound(); });

	RunProduce();
}

#pragma endregion

#pragma region PROTECTED

void UCProduceComponent::TickComponent(float InDeltaTime, ELevelTick InTickType, FActorComponentTickFunction* InThisTickFunction)
{
	Super::TickComponent(InDeltaTime, InTickType, InThisTickFunction);

	if(bIsProduce)
	{
		LeftProduceTime -= InDeltaTime;

		if((int)LastLeftProduceTime > (int)LeftProduceTime)
		{
			OnUpdateProduceTimeDelegate.ExecuteIfBound(LeftProduceTime + 1);
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Black, "Left:" + FString::SanitizeFloat(LeftProduceTime + 1.f));
		}

		if(LeftProduceTime <= 0.f)
		{
			Extract();
			RunProduce();
		}

		LastLeftProduceTime = LeftProduceTime;
	}
}

#pragma endregion

#pragma region PRIVATE

bool UCProduceComponent::CanProduce() const
{
	return !bIsProduce && Produce.MaxProductsInBuilding > ReadyProduct && AreReasourcesInGround() && AreResourcesInBuilding();
}

bool UCProduceComponent::AreReasourcesInGround() const
{
	return (Produce.ResourcesNeedInGround != EResourcesType::E_NONE && ResourcesInGround.IsValid()) || (LeftFreeProductsToExtract > 0);
}

bool UCProduceComponent::AreResourcesInBuilding() const
{
	for(const auto NeedResource : Produce.ResourcesNeedToCreateProduct)
		if(ResourcesInBuild[NeedResource.Key] < NeedResource.Value)
			return false;

	return true;
}

void UCProduceComponent::Extract()
{
	if(Produce.ResourcesNeedInGround != EResourcesType::E_NONE && ResourcesInGround.IsValid())
	{
		ReadyProduct += ResourcesInGround->Extract(Produce.CountProductByCycleWork - ReadyProduct);
	} else
	{
		int NewProducts = FMath::Min(Produce.CountProductByCycleWork - ReadyProduct, LeftFreeProductsToExtract - ReadyProduct);

		LeftFreeProductsToExtract -= NewProducts;

		ReadyProduct += NewProducts;

		if(LeftFreeProductsToExtract <= 0)
			OnEndResourcesDelegate.ExecuteIfBound();
	}

	OnReadyProductDelegate.ExecuteIfBound(ReadyProduct);
	bIsProduce = false;
}

#pragma endregion