
#include "CCameraInputComponent.h"
#include "../../Player/CBasePlayerPawn.h"

#pragma region PUBLIC

UCCameraInputComponent::UCCameraInputComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UCCameraInputComponent::BindInput(APawn* InPawn, UInputComponent* InInputComponent)
{
	Unbind();

	BasePlayerPawn = Cast<ACBasePlayerPawn>(InPawn);
	InputComponent = InInputComponent;

	if(BasePlayerPawn.IsValid() && InputComponent.IsValid())
	{
		InputComponent->BindAxis(VerticalRotationInputName, this, &UCCameraInputComponent::OnVerticalRotationInput);
		InputComponent->BindAxis(HorizontalRotationInputName, this, &UCCameraInputComponent::OnHorizontalRotationInput);
		InputComponent->BindAxis(HorizontalRotationInputName, this, &UCCameraInputComponent::OnHorizontalRotationInput);

		InputComponent->BindAxis(ForwardMoveInputName, this, &UCCameraInputComponent::OnForwardMoveInput);
		InputComponent->BindAxis(RightMoveInputName, this, &UCCameraInputComponent::OnRightMoveInput);
		InputComponent->BindAxis(UpMoveInputName, this, &UCCameraInputComponent::OnUpdMoveInput);

		OnUpdateVerticalRotationDelegate.BindUObject(BasePlayerPawn.Get(), &ACBasePlayerPawn::CameraRotationVertical);
		OnUpdateHorizontalRotationDelegate.BindUObject(BasePlayerPawn.Get(), &ACBasePlayerPawn::CameraRotationHorizontal);
		
		OnUpdateForwardMoveDelegate.BindUObject(BasePlayerPawn.Get(), &ACBasePlayerPawn::CameraMoveForward);
		OnUpdateRightMoveDelegate.BindUObject(BasePlayerPawn.Get(), &ACBasePlayerPawn::CameraMoveRight);
		OnUpdateUpMoveDelegate.BindUObject(BasePlayerPawn.Get(), &ACBasePlayerPawn::CameraMoveUp);
	}
}


#pragma endregion

#pragma region PRIVATE

void UCCameraInputComponent::Unbind()
{
	OnUpdateVerticalRotationDelegate.Unbind();
	OnUpdateHorizontalRotationDelegate.Unbind();

	OnUpdateForwardMoveDelegate.Unbind();
	OnUpdateRightMoveDelegate.Unbind();
	OnUpdateUpMoveDelegate.Unbind();
}

void UCCameraInputComponent::OnVerticalRotationInput(float InValue)
{
	if(bIsActive && InValue != 0.f)
		OnUpdateVerticalRotationDelegate.ExecuteIfBound(InValue);
}

void UCCameraInputComponent::OnHorizontalRotationInput(float InValue)
{
	if(bIsActive && InValue != 0.f)
		OnUpdateHorizontalRotationDelegate.ExecuteIfBound(InValue);
}

void UCCameraInputComponent::OnForwardMoveInput(float InValue)
{
	if(bIsActive && InValue != 0.f)
		OnUpdateForwardMoveDelegate.ExecuteIfBound(InValue);
}

void UCCameraInputComponent::OnRightMoveInput(float InValue)
{
	if(bIsActive && InValue != 0.f)
		OnUpdateRightMoveDelegate.ExecuteIfBound(InValue);
}

void UCCameraInputComponent::OnUpdMoveInput(float InValue)
{
	if(bIsActive && InValue != 0.f)
		OnUpdateUpMoveDelegate.ExecuteIfBound(InValue);
}

#pragma endregion