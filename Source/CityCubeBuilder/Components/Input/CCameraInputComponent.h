
#pragma once

#include "Components/ActorComponent.h"
#include "CCameraInputComponent.generated.h"

DECLARE_DELEGATE_OneParam(FOnUpdateAxisValueDelegate, float);

UCLASS(ClassGroup = (ClassGroup = "City cube builder"), meta = (BlueprintSpawnableComponent))
class UCCameraInputComponent: public UActorComponent
{
	GENERATED_BODY()

public:
	UCCameraInputComponent();

	void BindInput(class APawn* InPawn, class UInputComponent* InInputComponent);

	void SetIsActive(bool bInIsActive) { bIsActive = bInIsActive; }

private:
	FOnUpdateAxisValueDelegate OnUpdateVerticalRotationDelegate;
	FOnUpdateAxisValueDelegate OnUpdateHorizontalRotationDelegate;
	
	FOnUpdateAxisValueDelegate OnUpdateForwardMoveDelegate;
	FOnUpdateAxisValueDelegate OnUpdateRightMoveDelegate;
	FOnUpdateAxisValueDelegate OnUpdateUpMoveDelegate;

	void Unbind();

	void OnVerticalRotationInput(float InValue);
	void OnHorizontalRotationInput(float InValue);

	void OnForwardMoveInput(float InValue);
	void OnRightMoveInput(float InValue);
	void OnUpdMoveInput(float InValue);

	UPROPERTY(EditAnywhere, Category = "Camera input", meta = (AllowPrivateAccess = "true"))
		FName VerticalRotationInputName{NAME_None};
	UPROPERTY(EditAnywhere, Category = "Camera input", meta = (AllowPrivateAccess = "true"))
		FName HorizontalRotationInputName{NAME_None};
	
	UPROPERTY(EditAnywhere, Category = "Camera input", meta = (AllowPrivateAccess = "true"))
		FName ForwardMoveInputName{NAME_None};
	UPROPERTY(EditAnywhere, Category = "Camera input", meta = (AllowPrivateAccess = "true"))
		FName RightMoveInputName{NAME_None};
	UPROPERTY(EditAnywhere, Category = "Camera input", meta = (AllowPrivateAccess = "true"))
		FName UpMoveInputName{NAME_None};


	TWeakObjectPtr<class UInputComponent> InputComponent{nullptr};
	TWeakObjectPtr<class ACBasePlayerPawn> BasePlayerPawn{nullptr};

	bool bIsActive{true};
};
