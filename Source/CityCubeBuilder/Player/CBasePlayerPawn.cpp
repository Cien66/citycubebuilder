
#include "CBasePlayerPawn.h"

#include <Runtime/Engine/Classes/Camera/CameraComponent.h>

#pragma region PUBLIC

ACBasePlayerPawn::ACBasePlayerPawn()
{
	PrimaryActorTick.bCanEverTick = true;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	RootComponent = CameraComponent;
}

ACBasePlayerPawn::~ACBasePlayerPawn()
{}

void ACBasePlayerPawn::CameraRotationHorizontal(float InDirection)
{
	float DeltaSeconds = GetWorld() ? GetWorld()->GetDeltaSeconds() : 1.f;
	const FRotator& NewRotation = RootComponent->GetComponentRotation() + FRotator(0.f, InDirection * CameraVerticalRotationSpeed * DeltaSeconds, 0.f );

	RootComponent->SetWorldRotation(NewRotation);
}

void ACBasePlayerPawn::CameraRotationVertical(float InDirection)
{
	const FRotator& LastRotation = RootComponent->GetComponentRotation();

	float DeltaSeconds = GetWorld() ? GetWorld()->GetDeltaSeconds() : 1.f;
	float NewPitch = LastRotation.Pitch + InDirection * CameraHorizontalRotationSpeed * DeltaSeconds;

	NewPitch = FMath::Clamp(NewPitch, VerticalRotationClamp.X, VerticalRotationClamp.Y);

	const FRotator& NewRotation = FRotator(NewPitch, LastRotation.Yaw, LastRotation.Roll);

	RootComponent->SetWorldRotation(NewRotation);
}

void ACBasePlayerPawn::CameraMoveForward(float InDirection)
{
	const FVector& Forward = FVector::CrossProduct(-FVector::CrossProduct(RootComponent->GetForwardVector(), FVector::UpVector), FVector::UpVector);
	MoveCamera(Forward, InDirection * CameraForwardMoveSpeed);
}

void ACBasePlayerPawn::CameraMoveRight(float InDirection)
{
	MoveCamera(RootComponent->GetRightVector(), InDirection * CameraRightMoveSpeed);
}

void ACBasePlayerPawn::CameraMoveUp(float InDirection)
{
	MoveCamera(RootComponent->GetUpVector(), InDirection * CameraUpMoveSpeed);	
}

#pragma endregion

#pragma region PROTECTED

#pragma endregion

#pragma region PRIVATE

void ACBasePlayerPawn::MoveCamera(const FVector& InDirection, const float InSpeed)
{
	float DeltaSeconds = GetWorld() ? GetWorld()->GetDeltaSeconds() : 1.f;
	const FVector& NewLocation = RootComponent->GetComponentLocation() + InDirection * InSpeed * DeltaSeconds;

	RootComponent->SetWorldLocation(NewLocation);
}

#pragma endregion
