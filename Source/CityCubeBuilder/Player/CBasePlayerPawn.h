
#pragma once

#include "GameFramework/Pawn.h"
#include "../Data/CGameplayEnums.h"
#include "../Building/CBuildingActor.h"
#include "CBasePlayerPawn.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateCursorPositionDelegate, FHitResult&, OutHit, const EGameplayState&, InGameplayState);

UCLASS(ClassGroup = "City cube builder")
class ACBasePlayerPawn: public APawn
{
	GENERATED_BODY()

public:
	ACBasePlayerPawn();
	virtual ~ACBasePlayerPawn();

	void CameraRotationHorizontal(float InDirection);
	void CameraRotationVertical(float InDirection);
	
	void CameraMoveForward(float InDirection);
	void CameraMoveRight(float InDirection);
	void CameraMoveUp(float InDirection);

private:
	void MoveCamera(const FVector& InDirection, const float InSpeed);

	UPROPERTY(EditAnywhere, Category = "BasePlayerPawn", meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* CameraComponent{nullptr};

	UPROPERTY(EditAnywhere, Category = "BasePlayerPawn", meta = (AllowPrivateAccess = "true"))
		float CameraHorizontalRotationSpeed{180.f};
	UPROPERTY(EditAnywhere, Category = "BasePlayerPawn", meta = (AllowPrivateAccess = "true"))
		float CameraVerticalRotationSpeed{180.f};

	UPROPERTY(EditAnywhere, Category = "BasePlayerPawn", meta = (AllowPrivateAccess = "true"))
		float CameraForwardMoveSpeed{180.f};
	UPROPERTY(EditAnywhere, Category = "BasePlayerPawn", meta = (AllowPrivateAccess = "true"))
		float CameraRightMoveSpeed{180.f};
	UPROPERTY(EditAnywhere, Category = "BasePlayerPawn", meta = (AllowPrivateAccess = "true"))
		float CameraUpMoveSpeed{180.f};
	
	UPROPERTY(EditAnywhere, Category = "BasePlayerPawn", meta = (AllowPrivateAccess = "true"))
		FVector2D VerticalRotationClamp{FVector2D::ZeroVector};
};
