
#pragma once

#include "../Data/CGameplayEnums.h"
#include "GameFramework/PlayerController.h"
#include "CBasePlayerController.generated.h"

DECLARE_DELEGATE(FOnInputPressedDelegate);

UCLASS(ClassGroup = "City cube builder")
class ACBasePlayerController: public APlayerController
{
	GENERATED_BODY()

public:
	ACBasePlayerController();
	~ACBasePlayerController();

	UFUNCTION(BlueprintCallable, Category = "BasePlayerController")
		void CreateGhostBuilding(const FString& InName);
	UFUNCTION(BlueprintCallable, Category = "BasePlayerController")
		void CreateRoad(const FString& InName) { /*OnCreateRoadDelegate.ExecuteIfBound(InName);*/ }

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type InEndPlayReason) override;

	virtual void SetPawn(APawn* InPawn) override;
	virtual void SetupInputComponent() override;

	UPROPERTY(EditAnywhere, Category = "BasePlayerController")
		FName EnterInputName {NAME_None};
	UPROPERTY(EditAnywhere, Category = "BasePlayerController")
		FName CancelInputName{NAME_None};

private:
	void OnEnterInput();
	void OnCancelInput();

	UFUNCTION()
		void UpdateTraceUnderMouse(FVector& OutLocation);

	UPROPERTY(EditAnywhere, Category = "BasePlayerController")
		TEnumAsByte<ETraceTypeQuery> TraceFreeCursorChannel{ETraceTypeQuery::TraceTypeQuery1};
	UPROPERTY(EditAnywhere, Category = "BasePlayerController")
		TEnumAsByte<ETraceTypeQuery> TraceBuildCursorChannel{ETraceTypeQuery::TraceTypeQuery1};

	UPROPERTY(EditAnywhere, Category = "BasePlayerController")
		bool bTraceComplex{false};

	UPROPERTY(EditAnywhere, Category = "BasePlayerPawn")
		class UDataTable* BuildingsDataTable{nullptr};

	UPROPERTY(VisibleAnywhere, Category = "BasePlayerPawn")
		class UCBuildSystemComponent* BuildSystemComponent{nullptr};

	UPROPERTY(VisibleAnywhere, Category = "BasePlayerPawn")
		class UCCameraInputComponent* CameraInputComponent{nullptr};

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BasePlayerPawn", meta = (AllowPrivateAccess = "true"))
		class UCUIHandlerComponent* UIHandlerComponent{nullptr};

	FOnInputPressedDelegate OnEnterPressedDelegate;
	FOnInputPressedDelegate OnCancelPressedDelegate;


	TWeakObjectPtr<class ACBuildingFactoryActor> BuildingFactory{nullptr}; 
	TWeakObjectPtr<class ACWarehousesControllerActor> WarehousesController{nullptr};
	TWeakObjectPtr<class ACEconomyControllerActor> EconomyController{nullptr};

	EGameplayState InGameplayState{EGameplayState::E_Free};
};
