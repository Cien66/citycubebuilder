

#include "CBasePlayerController.h"
#include "CBasePlayerPawn.h"

#include "../Factory/CBuildingFactoryActor.h"
#include "../Components/BuildSystem/CBuildSystemComponent.h"

#include "../Controllers/CWarehousesControllerActor.h"
#include "../Controllers/CEconomyControllerActor.h"

#include "../Components/Input/CCameraInputComponent.h"
#include "../Components/UIHandler/CUIHandlerComponent.h"

#pragma region PUBLIC

ACBasePlayerController::ACBasePlayerController()
{
	BuildSystemComponent = CreateDefaultSubobject<UCBuildSystemComponent>(TEXT("BuildSystemComponent"));
	CameraInputComponent = CreateDefaultSubobject<UCCameraInputComponent>(TEXT("CameraInputComponent"));
	UIHandlerComponent = CreateDefaultSubobject<UCUIHandlerComponent>(TEXT("UIHandlerComponent"));
}

ACBasePlayerController::~ACBasePlayerController()
{
}

void ACBasePlayerController::CreateGhostBuilding(const FString& InName)
{
	BuildSystemComponent->CreateGhostBuilding(InName);
}

#pragma endregion

#pragma region PROTECTED

void ACBasePlayerController::BeginPlay()
{
	Super::BeginPlay();

	BuildSystemComponent->OnUpdateBuildingLocationDelegate.BindUObject(this, &ACBasePlayerController::UpdateTraceUnderMouse);

	if(UWorld* World = GetWorld())
	{
		FActorSpawnParameters ASP;
		ASP.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		WarehousesController = World->SpawnActor<ACWarehousesControllerActor>(ASP);
		EconomyController = World->SpawnActor<ACEconomyControllerActor>(ASP);
		BuildingFactory = World->SpawnActor<ACBuildingFactoryActor>(ASP);

		if(EconomyController.IsValid())
			EconomyController->SetWarehouseController(WarehousesController);
	}
		
	BuildSystemComponent->SetBuildingFactory(BuildingFactory);
	BuildSystemComponent->SetWarehousesController(WarehousesController);

	UIHandlerComponent->Init(WarehousesController);
}

void ACBasePlayerController::EndPlay(const EEndPlayReason::Type InEndPlayReason)
{
	Super::EndPlay(InEndPlayReason);

	BuildSystemComponent->OnUpdateBuildingLocationDelegate.Unbind();
}

void ACBasePlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	CameraInputComponent->BindInput(InPawn, InputComponent);
}

void ACBasePlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	check(InputComponent);

	InputComponent->BindAction(EnterInputName, EInputEvent::IE_Pressed, this, &ACBasePlayerController::OnEnterInput);
	InputComponent->BindAction(CancelInputName, EInputEvent::IE_Pressed, this, &ACBasePlayerController::OnCancelInput);
}

#pragma endregion

#pragma region PRIVATE

void ACBasePlayerController::UpdateTraceUnderMouse(FVector& OutLocation)
{
	ETraceTypeQuery TraceChannel = InGameplayState == EGameplayState::E_Build ? TraceBuildCursorChannel : TraceFreeCursorChannel;

	FHitResult Hit;
	GetHitResultUnderCursorByChannel(TraceBuildCursorChannel, bTraceComplex, Hit);
	OutLocation = Hit.ImpactPoint;
}

void ACBasePlayerController::OnEnterInput()
{
	TWeakObjectPtr<ACBuildingActor> RealBuilding = BuildSystemComponent->BuildRealBuilding();
}

void ACBasePlayerController::OnCancelInput()
{
	OnCancelPressedDelegate.ExecuteIfBound();
}

#pragma endregion