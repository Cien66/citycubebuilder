
#pragma once

#include "GameFramework/Actor.h"
#include "CPathActor.generated.h"

UCLASS(ClassGroup = "City cube builder")
class ACCrossPathActor: public AActor
{
	GENERATED_BODY()

public:
	ACCrossPathActor() {}

private:
	UPROPERTY(VisibleAnywhere, Category = "RoadPath")
		class USphereComponent* SphereComponent{nullptr};

	TArray<TWeakObjectPtr<ACPathActor>> Paths;
};

UCLASS(ClassGroup = "City cube builder")
class ACPathActor: public AActor
{
	GENERATED_BODY()

public:
	ACPathActor();


private:
	TArray<TWeakObjectPtr<ACCrossPathActor>> Crosses;
};
