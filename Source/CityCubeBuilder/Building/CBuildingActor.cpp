
#include "CBuildingActor.h"
#include "../AI/CAICharacter.h"

#include <Runtime/Engine/Classes/Components/BoxComponent.h>

#pragma region PUBLIC

ACBuildingActor::ACBuildingActor()
{
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	RootComponent = BoxComponent;	

	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ACBuildingActor::BeginOverlap);
}

ACBuildingActor::~ACBuildingActor()
{
}

#pragma endregion

#pragma region PROTECTED

void ACBuildingActor::SetParameters_Implementation(const FCBuilding& InBuilding)
{
	Building = InBuilding;

	OnSetParameters(InBuilding);
}

void ACBuildingActor::SetIsGhost_Implementation(bool bInIsGhost)
{
	bIsGhost = bInIsGhost;

	BoxComponent->SetCollisionEnabled(bIsGhost ? ECollisionEnabled::NoCollision : ECollisionEnabled::QueryAndPhysics);

	OnSetGhost(bInIsGhost);
}

void ACBuildingActor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	BoxComponent->OnComponentBeginOverlap.RemoveDynamic(this, &ACBuildingActor::BeginOverlap);

	Super::EndPlay(EndPlayReason);
}

#pragma region PRIVATE

void ACBuildingActor::BeginOverlap(UPrimitiveComponent* InOverlappedComponent, AActor* InOtherActor, UPrimitiveComponent* InOtherComp, int32 InOtherBodyIndex, bool InbFromSweep, const FHitResult& InSweepResult)
{
	if(ACAICharacter* AICharacter = Cast<ACAICharacter>(InOtherActor))
		OnEnterAICharacter(AICharacter);
}

#pragma endregion

