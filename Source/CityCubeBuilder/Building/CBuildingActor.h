
#pragma once

#include "GameFramework/Actor.h"
#include "../Data/CGameplayEnums.h"
#include "../Data/CGameplayStuctures.h"
#include "CBuildingActor.generated.h"

class UBoxComponent; 

UCLASS(Abstract, ClassGroup = "City cube builder")
class ACBuildingActor: public AActor
{
	GENERATED_BODY()

public:
	ACBuildingActor();
	virtual ~ACBuildingActor();

	const UBoxComponent* GetBoxComponent() { return BoxComponent; }

	UFUNCTION(BlueprintNativeEvent, Category = "Building")
		void SetParameters(const FCBuilding& InBuilding);

	UFUNCTION(BlueprintNativeEvent, Category = "Building")
		void SetIsGhost(bool bInIsGhost);

	UFUNCTION(BlueprintImplementableEvent, Category = "Building")
		void UpdateGhostOnCollisionBuild(bool bInCanBuild);

	UFUNCTION(BlueprintCallable, Category = "Building")
		FORCEINLINE FCBuilding& GetParameters() { return Building; }
	
protected:
	virtual void OnSetGhost(bool bInIsGhost) {};
	virtual void OnSetParameters(const FCBuilding& InBuilding) {};

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Building")
		FCBuilding Building;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Building")
		UBoxComponent* BoxComponent{nullptr};

private:
	bool bIsGhost{false};

	UFUNCTION()
		void BeginOverlap(UPrimitiveComponent* InOverlappedComponent, AActor* InOtherActor, UPrimitiveComponent* InOtherComp, int32 InOtherBodyIndex, bool InbFromSweep, const FHitResult& InSweepResult);

	virtual void OnEnterAICharacter(class ACAICharacter* InAICharacter) {}
};
