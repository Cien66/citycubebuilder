
#pragma once

#include "CBuildingActor.h"
#include "../Data/CDelegates.h"
#include "CWarehouseBuildingActor.generated.h"


class UCWarehouseComponent;

UCLASS(ClassGroup = "City cube builder")
class ACWarehouseBuildingActor : public ACBuildingActor
{
	GENERATED_BODY()

public:
	ACWarehouseBuildingActor();

	FOnUpdateProductDelegate OnDeliverProductDelegate;
	FOnUpdateProductDelegate OnTakeProductDelegate;

	const UCWarehouseComponent* GetWarehouseComponent() { return WarehouseComponent; }

	void AddBuildingWaitingForResources(const TWeakObjectPtr<const ACBuildingActor>& InNewBuilding, const EResourcesType& InResourcesType, float InCount);

protected:
	virtual void OnSetParameters(const FCBuilding& InBuilding) override;

	void AddToWaitingBuilding(const TWeakObjectPtr<const ACBuildingActor>& InNewBuilding, const EResourcesType& InResourcesType, float InCount);

	const TWeakObjectPtr<class ACAICharacter> GetFreeAICharacter() const;

	UCWarehouseComponent* WarehouseComponent{nullptr};

	TMap<const TWeakObjectPtr<const ACBuildingActor>, TMap<const EResourcesType, float>> BuildingWaitingForResources;

	TArray<const TWeakObjectPtr<class ACAICharacter>> Workers;
};
