
#pragma once

#include "CBuildingActor.h"
#include "../Data/CDelegates.h"
#include "../Interfaces/CSetGroundResourcesInterface.h"
#include "CMineBuildingActor.generated.h"


UCLASS(ClassGroup = "City cube builder")
class ACMineBuildingActor: public ACBuildingActor, public ICSetGroundResourcesInterface
{
	GENERATED_BODY()

public:
	ACMineBuildingActor();

	FOnUpdateProductDelegate OnCreateNewProductsDelegate;
	FOnUpdateProductDelegate OnNeedResourcesDelegate;

	FDelegateHandle OnCreateNewProductsDelegateHandle;
	FDelegateHandle OnNeedResourcesDelegateHandle;

	FORCEINLINE void AddResources(const TMap<EResourcesType, float>& InResources);
	virtual void SetGroundResources(class ACResourcesActor* InGroundResources) override;

protected:
	virtual void BeginPlay() override;
	virtual void OnSetGhost(bool bInIsGhost) override;
	virtual void OnSetParameters(const FCBuilding& InBuilding)override;

	UFUNCTION(BlueprintNativeEvent, Category = "Mine")
		void OnProductDone(float InProducts);

	UFUNCTION(BlueprintImplementableEvent, Category = "Mine")
		void OnEndGroundResources();

	UFUNCTION(BlueprintImplementableEvent, Category = "Mine")
		void OnUpdateWorkTime(int InLeftWorkTime);

private:
	class UCProduceComponent* ProduceComponent{nullptr};
};
