

#include "CMineBuildingActor.h"
#include "../Components/Buildings/CProduceComponent.h"

#pragma region PUBLIC

ACMineBuildingActor::ACMineBuildingActor()
{
	PrimaryActorTick.bCanEverTick = true;

	ProduceComponent = CreateDefaultSubobject<UCProduceComponent>(TEXT("ProduceComponent"));
}

void ACMineBuildingActor::AddResources(const TMap<EResourcesType, float>& InResources)
{
	ProduceComponent->AddResources(InResources);
}

void ACMineBuildingActor::SetGroundResources(ACResourcesActor* InGroundResources)
{
	ProduceComponent->SetResourcesInGround(InGroundResources);
}

#pragma endregion

#pragma region PROTECTED

void ACMineBuildingActor::BeginPlay()
{
	Super::BeginPlay();

	ProduceComponent->OnReadyProductDelegate.BindUObject(this, &ACMineBuildingActor::OnProductDone);
	ProduceComponent->OnEndResourcesDelegate.BindUObject(this, &ACMineBuildingActor::OnEndGroundResources);
	ProduceComponent->OnUpdateProduceTimeDelegate.BindUObject(this, &ACMineBuildingActor::OnUpdateWorkTime);
}

void ACMineBuildingActor::OnSetGhost(bool bInIsGhost)
{
	ProduceComponent->SetComponentTickEnabled(!bInIsGhost);
}

void ACMineBuildingActor::OnSetParameters(const FCBuilding& InBuilding)
{
	ProduceComponent->SetParameters(Building.Produce);
}

void ACMineBuildingActor::OnProductDone_Implementation(float InProducts)
{
	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Blue, "InProducts: " + FString::SanitizeFloat(InProducts));
	OnCreateNewProductsDelegate.Broadcast(ProduceComponent->GetParameters().ProductType, InProducts, this);
}

#pragma endregion