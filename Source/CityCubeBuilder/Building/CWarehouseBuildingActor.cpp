
#include "CWarehouseBuildingActor.h"
#include "../Components/Buildings/CWarehouseComponent.h"

#include "../AI/CAICharacter.h"


#pragma region PUBLIC

ACWarehouseBuildingActor::ACWarehouseBuildingActor()
{
	WarehouseComponent = CreateDefaultSubobject<UCWarehouseComponent>(TEXT("WarehouseComponent"));
}

void ACWarehouseBuildingActor::AddBuildingWaitingForResources(const TWeakObjectPtr<const ACBuildingActor>& InNewBuilding, const EResourcesType& InResourcesType, float InCount)
{
	TWeakObjectPtr<ACAICharacter> FreeAI = GetFreeAICharacter();

	if(FreeAI.IsValid())
	{
		float RealCount = FMath::Min(FreeAI->GetMaxResourcesHave(), InCount);
		OnTakeProductDelegate.Broadcast(InResourcesType, RealCount, InNewBuilding.Get());

		if(RealCount > 0)
		{ 
			FreeAI->SetIsFree(false);

			float LeftNeededReourcesSources = InCount - RealCount;

			if(LeftNeededReourcesSources > 0)
				AddToWaitingBuilding(InNewBuilding, InResourcesType, LeftNeededReourcesSources);

			FreeAI->TakeResources(InResourcesType, RealCount);
		}
	}
}

#pragma endregion

#pragma region PRIVATE

void ACWarehouseBuildingActor::OnSetParameters(const FCBuilding& InBuilding)
{
	WarehouseComponent->SetParameters(InBuilding.Produce.MaxProductsInBuilding);
}

void ACWarehouseBuildingActor::AddToWaitingBuilding(const TWeakObjectPtr<const ACBuildingActor>& InNewBuilding, const EResourcesType& InResourcesType, float InCount)
{
	if(TMap<const EResourcesType, float>* NeededResources = BuildingWaitingForResources.Find(InNewBuilding))
	{
		NeededResources->Find(InResourcesType) ?
			(*NeededResources)[InResourcesType] += InCount :
			BuildingWaitingForResources[InNewBuilding].Add(InResourcesType, InCount);
	} else
	{
		TMap<const EResourcesType, float>Resources;
		Resources.Add(InResourcesType, InCount);
		BuildingWaitingForResources.Add(InNewBuilding, Resources);
	}
}

const TWeakObjectPtr<class ACAICharacter> ACWarehouseBuildingActor::GetFreeAICharacter() const
{
	TWeakObjectPtr<class ACAICharacter> FreeWorker = nullptr;
	for(int i = 0; i < Workers.Num(); ++i)
	{
		if(Workers[i]->GetIsFree())
		{
			FreeWorker = Workers[i];
			return FreeWorker;
		}
	}

	return FreeWorker;
}

#pragma endregion
