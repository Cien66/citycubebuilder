
#pragma once

#include "CFactoryActor.h"
#include "CBuildingFactoryActor.generated.h"

UCLASS(ClassGroup = "City cube builder")
class ACBuildingFactoryActor: public ACFactoryActor
{
	GENERATED_BODY()

public:
	virtual class ACBuildingActor* Spawn(const FString& InBuildingName, const FTransform& InSpawnTransform, bool bInISGhost) override;

private:
	class ACBuildingActor* ExecuteSpawn(const struct FCBuildingDataTable* InBuildingDataTable, const FTransform& InSpawnTransform, bool bInISGhost);
};
