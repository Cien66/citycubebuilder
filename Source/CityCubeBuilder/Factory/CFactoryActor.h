#pragma once

#include "GameFramework/Actor.h"
#include <Runtime/Engine/Classes/Engine/DataTable.h>
#include "CFactoryActor.generated.h"

UCLASS(Abstract, ClassGroup = "City cube builder")
class ACFactoryActor: public AActor
{
	GENERATED_BODY()

public:
	ACFactoryActor();

	virtual class ACBuildingActor* Spawn(const FString& InBuildingName, const FTransform& InSpawnTransform, bool bInISGhost) { return nullptr; }

	void SetDataTable(TWeakObjectPtr<class UDataTable> InDataTable) { ParametersDataTable = InDataTable; }

protected:	
	TWeakObjectPtr<class UDataTable> ParametersDataTable{nullptr};
};
