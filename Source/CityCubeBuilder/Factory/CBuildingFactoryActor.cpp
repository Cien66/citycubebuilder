
#include "CBuildingFactoryActor.h"

#include "../Building/CBuildingActor.h"
#include "../Data/CBuildingDataTable.h"
#include "../Controllers/CWarehousesControllerActor.h"

#pragma region PUBLIC

ACBuildingActor* ACBuildingFactoryActor::Spawn(const FString& InBuildingName, const FTransform& InSpawnTransform, bool bInISGhost)
{
	if(ParametersDataTable.IsValid())
		if(FCBuildingDataTable* Parameters = ParametersDataTable->FindRow<FCBuildingDataTable>(*InBuildingName, "ACBuildingFactoryActor::Spawn: " + InBuildingName, true))
			return ExecuteSpawn(Parameters, InSpawnTransform, bInISGhost);

	return nullptr;
}

#pragma endregion

#pragma region PRIVATE

ACBuildingActor* ACBuildingFactoryActor::ExecuteSpawn(const FCBuildingDataTable* InBuildingDataTable, const FTransform& InSpawnTransform, bool bInISGhost)
{
	if(UWorld* World = GetWorld())
	{
		FActorSpawnParameters ASP;
		ASP.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		ACBuildingActor* BuildingActor = World->SpawnActor<ACBuildingActor>(InBuildingDataTable->Building.BuildingClass, InSpawnTransform, ASP);
		BuildingActor->SetParameters(InBuildingDataTable->Building);
		BuildingActor->SetIsGhost(bInISGhost);

		return BuildingActor;
	}

	return nullptr;
}

#pragma endregion
