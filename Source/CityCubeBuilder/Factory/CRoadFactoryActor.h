
#pragma once

#include "CFactoryActor.h"
#include "CRoadFactoryActor.generated.h"

UCLASS(ClassGroup = "City cube builder")
class ACRoadFactoryActor: public ACFactoryActor
{
	GENERATED_BODY()

public:
	virtual class ACBuildingActor* Spawn(const FString& InBuildingName, const FTransform& InSpawnTransform, bool bInISGhost) override;
};
