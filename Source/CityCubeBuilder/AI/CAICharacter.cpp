
#include "CAICharacter.h"

float ACAICharacter::DeliverResources(const EResourcesType& InResources, const float& InCount)
{
	if(ResourcesType == InResources)
	{
		float RealCount = FMath::Min(InCount, ResourcesCount);
		ResourcesCount -= RealCount;

		if(ResourcesCount <= 0.f)
			ResourcesType = EResourcesType::E_NONE;

		OnDeliverResources(ResourcesType, ResourcesCount);

		return RealCount;
	}

	return 0.0f;
}

float ACAICharacter::TakeResources(const EResourcesType& InResources, const float& InCount)
{
	float Left = InCount;

	if(ResourcesType == EResourcesType::E_NONE || (ResourcesType == InResources && ResourcesCount < MaxResourcesHave))
	{
		float Diff = FMath::Min(InCount, MaxResourcesHave - ResourcesCount);
		ResourcesCount = ResourcesCount + Diff;
		ResourcesType = InResources;

		OnAddResources(ResourcesType, ResourcesCount);
		
		Left = InCount - Diff;
	}

	return Left;
}
