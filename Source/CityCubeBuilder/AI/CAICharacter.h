
#pragma once

#include "GameFramework/Character.h"
#include "../Data/CGameplayEnums.h"
#include "CAICharacter.generated.h"

UCLASS(ClassGroup = "City cube builder")
class ACAICharacter: public ACharacter
{
	GENERATED_BODY()

public:
	ACAICharacter() {};

	virtual float TakeResources(const EResourcesType& InResources, const float& InCount);
	virtual float DeliverResources(const EResourcesType& InResources, const float& InCount);

	FORCEINLINE void SetIsFree(bool bInIsFree) { bIsFree = bInIsFree; }
	FORCEINLINE bool GetIsFree() const { return bIsFree; }

	float GetMaxResourcesHave() const { return MaxResourcesHave; }


	//TO POWINNO BY� W JAKIM� WORKERS_CONTROLLER'ZE 
	void MoveToBuilding(const TWeakObjectPtr<const class ACBuildingActor>& InBuilding) {};

protected:
	UFUNCTION(BlueprintimplementableEvent, Category = "AICharacter")
		void OnAddResources(const EResourcesType& InResources, const float& InCount);

	UFUNCTION(BlueprintimplementableEvent, Category = "AICharacter")
		void OnDeliverResources(const EResourcesType& InResources, const float& InCount);

	UPROPERTY(EditAnywhere, Category = "AICharacter")
		float MaxResourcesHave = 1.f;

	UPROPERTY(BlueprintReadOnly, Category = "AICharacter")
		EResourcesType ResourcesType{EResourcesType::E_NONE};
	UPROPERTY(BlueprintReadOnly, Category = "AICharacter")
		float ResourcesCount{0.f};

	UPROPERTY(BlueprintReadOnly, Category = "AICharacter")
		bool bIsFree{false};
};
